# Maths with Python

 [![PyPI version](https://badge.fury.io/py/maths-with-python.svg)](https://badge.fury.io/py/maths-with-python) 
 [![Build Status](https://travis-ci.org/LaGuer/maths-with-python.svg?branch=master)](https://travis-ci.org/LaGuer/maths-with-python) 
 [![codecov](https://codecov.io/gh/LaGuer/maths-with-python/branch/master/graph/badge.svg)](https://codecov.io/gh/LaGuer/maths-with-python) 
 [![Binder](http://mybinder.org/badge.svg)](http://mybinder.org/repo/LaGuer/maths-with-python)
 [![nbviewer](https://img.shields.io/badge/view%20on-nbviewer-brightgreen.svg)](https://nbviewer.jupyter.org/github/LaGuer/maths-with-python/blob/master/website-index.ipynb)

[![Documentation Status](https://readthedocs.org/projects/maths-with-python/badge/?version=latest)](http://maths-with-python.readthedocs.org/en/latest/?badge=latest)

* [HTML Format of the maths-with-python lecture](https://laguer.gitlab.io/maths-with-python/)

Gathering some material for a Python class aimed at first year undergraduate mathematicians at Southampton. To be covered in ten hours of labs.

To build these notes you will need

* python (3.3+)
* ipython (3+)
* ipython-notebook or jupyter (the notes recommend jupyter, but that's for forwards compatibility: it should work on ipython-notebook 3+)
* scipy stack (scipy, sympy, numpy, matplotlib)
* latex
* nose
* watermark line magic (see https://github.com/rasbt/watermark)
* memory profiler line magic (see https://pypi.python.org/pypi/memory_profiler)
